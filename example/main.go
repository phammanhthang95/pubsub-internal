package main

import (
	"context"
	internal_pubsub "gitlab.com/phammanhthang95/pubsub-internal"
	"gitlab.com/phammanhthang95/pubsub-internal/localpb"
	"log"
	"time"
)

func main() {
	var localPS internal_pubsub.Pubsub = localpb.NewPubSub()

	var topic internal_pubsub.Topic = "order.created"

	sub1, close1 := localPS.Subscribe(context.Background(), topic)
	sub2, _ := localPS.Subscribe(context.Background(), topic)

	localPS.Publish(context.Background(), topic, internal_pubsub.NewMessage(1))
	localPS.Publish(context.Background(), topic, internal_pubsub.NewMessage(2))

	go func() {
		for {
			log.Println("Sub 1:", (<-sub1).Data())
			time.Sleep(time.Millisecond * 400)
		}
	}()

	go func() {
		for {
			log.Println("Sub 2:", (<-sub2).Data())
			time.Sleep(time.Millisecond * 400)
		}
	}()

	time.Sleep(time.Second * 3)
	close1()
	//close2()
	//
	localPS.Publish(context.Background(), topic, internal_pubsub.NewMessage(3))
	//
	time.Sleep(time.Second * 2)
}
