package localpb

import (
	"context"
	internal_pubsub "gitlab.com/phammanhthang95/pubsub-internal"
	"log"
	"sync"
)

type localPubSub struct {
	messageQueue chan *internal_pubsub.Message
	mapChannel   map[internal_pubsub.Topic][]chan *internal_pubsub.Message
	locker       *sync.RWMutex
}

func NewPubSub() *localPubSub {
	pb := &localPubSub{
		messageQueue: make(chan *internal_pubsub.Message, 10000),
		mapChannel:   make(map[internal_pubsub.Topic][]chan *internal_pubsub.Message),
		locker:       new(sync.RWMutex),
	}

	pb.run()

	return pb
}


func (ps *localPubSub) Publish(ctx context.Context, topic internal_pubsub.Topic, data *internal_pubsub.Message) error {
	data.SetChannel(topic)

	go func() {
		ps.messageQueue <- data
		log.Println("New event published:", data.String(), "with data", data.Data())
	}()
	return nil
}


func (ps *localPubSub) Subscribe(ctx context.Context, topic internal_pubsub.Topic) (ch <-chan *internal_pubsub.Message, close func()) {
	c := make(chan *internal_pubsub.Message)

	ps.locker.Lock()

	if val, ok := ps.mapChannel[topic]; ok {
		val = append(ps.mapChannel[topic], c)
		ps.mapChannel[topic] = val
	} else {
		ps.mapChannel[topic] = []chan *internal_pubsub.Message{c}
	}

	ps.locker.Unlock()

	return c, func() {
		log.Println("Unsubscribe")

		if chans, ok := ps.mapChannel[topic]; ok {
			for i := range chans {
				if chans[i] == c {
					// remove element at index in chans
					chans = append(chans[:i], chans[i+1:]...)

					ps.locker.Lock()
					ps.mapChannel[topic] = chans
					ps.locker.Unlock()
					break
				}
			}
		}
	}

}

func (ps *localPubSub) run() error {
	log.Println("Pubsub started")

	go func() {
		for {
			mess := <-ps.messageQueue
			log.Println("Message dequeue:", mess.String())

			if subs, ok := ps.mapChannel[mess.Channel()]; ok {
				for i := range subs {
					go func(c chan *internal_pubsub.Message) {
						c <- mess
						//f(mess)
					}(subs[i])
				}
			}
			//else {
			//	ps.messageQueue <- mess
			//}
		}
	}()

	return nil
}
